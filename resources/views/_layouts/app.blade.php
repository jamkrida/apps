<!doctype html>
<html lang="en">
<head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <link rel="icon" href="{{ asset('assets/images/favicon.png')}}" type="image/x-icon"/>
    <link rel="shortcut icon" type="image/x-icon" href="{{ asset('assets/images/favicon.png')}}" />
    <!-- Bootstrap CSS -->
    <title>{{ env('APP_NAME', 'JAMKRIDAKALTIM')}}</title>
    @yield('top-script')
</head>
<body>
    @yield('base-content')
    @yield('bottom-script')
</body>
</html>
